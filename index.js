var express = require("express");
var app = express();
app.get("/", function (req, res) {
    res.send({ ui: "universitas" });
});
app.get("/hello", function (req, res) {
    res.send({ hello: "Hello, World!" });
});
var PORT = process.env.PORT || 5000;
app.listen(PORT, function () {
    console.log("App listening on port " + PORT);
});
