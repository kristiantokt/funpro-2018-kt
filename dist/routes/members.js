"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var members = require("../db.json"); // load our local database file
var Members = /** @class */ (function () {
    function Members() {
    }
    Members.prototype.routes = function (app) {
        app.route("/members")
            .get(function (req, res) {
            res.status(200).send(members);
        });
        app.route("/")
            .get(function (req, res) {
            res.status(200).send({ hello: "world" });
        });
    };
    return Members;
}());
exports.Members = Members;
//# sourceMappingURL=members.js.map