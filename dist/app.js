"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var express_1 = __importDefault(require("express"));
var members_1 = require("./routes/members");
var App = /** @class */ (function () {
    function App() {
        this.memberRoute = new members_1.Members();
        this.app = express_1.default(); // run the express instance and store in app
        // this.config();
        this.memberRoute.routes(this.app);
    }
    return App;
}());
exports.default = new App().app;
//# sourceMappingURL=app.js.map